import { Component } from '@angular/core';
import { ToastOptions } from '@Ionic/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  source: number[]; // to store all data
  data: number[]; // to store displayed data

  constructor(private toastCtrl: ToastController) { }

  ionViewWillEnter() {
    this.initData(); // Ex: get data from server...
  }

  doRefresh(event: any): void {
    setTimeout(() => {
      this.initData();
      event.target.complete();
    }, 2000);
  }

  loadData(event: any): void {
    setTimeout(() => {
      this.pushData();
      event.target.complete();
    }, 500);
  }

  *range(start: number, end: number) {
    for (let i = start; i <= end; i++) {
      yield i;
    }
  }

  initData(): void {
    // Ex: get data from server...
    this.source = Array.from(this.range(1, 103)); // Ex: All data returned from server
    this.data = this.source.slice(0, 20); // Displayed data
  }

  pushData() {
    const start = this.data.length;

    if (start < this.source.length) {
      let end = start + 10;
      end = end > this.source.length ? this.source.length : end;
      this.data = this.data.concat(this.source.slice(start, end));
    } else {
      const options: ToastOptions = {
        message: 'All data has been loaded',
        duration: 3000,
        mode: 'ios',
        position: 'bottom',
      };

      this.toastCtrl.create(options).then((toast: HTMLIonToastElement) => {
        toast.present();
      }).catch((e: any) => {
        console.error(e);
      });
    }
  }

}
